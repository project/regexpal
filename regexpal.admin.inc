<?php

/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


/**
 * Menu callback for the settings form.
 */
function regexpal_admin_form() {

  $form['regexpal_default_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings for RegexPal'),
    '#description' => t('You can test regular expression on this page: <a target=_blank href="!url">RegexPal</a>', array('!url' => url('regexpal'))) .
    t('<br>') . t('You can also configure <a target=_blank href="!url_perm">Permissions</a> to give access to RegexPal page for other users or you can configure as a <a target=_blank href="!url_block">Block</a>.', array('!url_perm' => url('admin/user/permissions'), '!url_block' => url('admin/build/block'))),
    '#collapsible' => TRUE,
  ); 

  $form['regexpal_default_settings']['regexpal_global'] = array(
    '#type' => 'checkbox',
    '#title' => t('Global.'),
    '#id' => 'flagG',
    //'#description' => t(''),
    '#default_value' => variable_get('regexpal_global', FALSE),
  );
  $form['regexpal_default_settings']['regexpal_case_insensitive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Case insensitive.'),
    '#id' => 'flagI',
    '#description' => t('If this modifier is set, letters in the pattern match both upper and lower case letters.'),
    '#default_value' => variable_get('regexpal_case_insensitive', FALSE),
  );
  $form['regexpal_default_settings']['regexpal_match_line_breaks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Match line breaks.'),
    '#id' => 'flagM',
    '#description' => t('By default, the subject string as consisting of a single "line" of characters (even if it actually contains several newlines). The "start of line" metacharacter (^) matches only at the start of the string, while the "end of line" metacharacter ($) matches only at the end of the string, or before a terminating newline (unless D modifier is set).'),
    '#default_value' => variable_get('regexpal_match_line_breaks', FALSE),
  );
  $form['regexpal_default_settings']['regexpal_dot_matches_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dot matches all.'),
    '#id' => 'flagS',
    '#description' => t('If this modifier is set, a dot metacharacter in the pattern matches all characters, including newlines. Without it, newlines are excluded. This modifier is equivalent to Perl\'s /s modifier. A negative class such as [^a] always matches a newline character, independent of the setting of this modifier.'),
    '#default_value' => variable_get('regexpal_dot_matches_all', FALSE),
  );

  $form['regexpal_default_settings_other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other settings for RegexPal'),
    '#collapsible' => TRUE,
  ); 
  $form['regexpal_default_settings_other']['regexpal_input_rows'] = array(
    '#type' => 'number',
    '#title' => t('Number of %ptype for %wtype textarea.', array('%ptype' => 'rows', '%wtype' => 'input')),
    '#default_value' => variable_get('regexpal_input_rows', 3),
  );
  $form['regexpal_default_settings_other']['regexpal_input_cols'] = array(
    '#type' => 'number',
    '#title' => t('Number of %ptype for %wtype textarea.', array('%ptype' => 'columns', '%wtype' => 'input')),
    '#default_value' => variable_get('regexpal_input_cols', 80),
  );
  $form['regexpal_default_settings_other']['regexpal_output_rows'] = array(
    '#type' => 'number',
    '#title' => t('Number of %ptype for %wtype textarea.', array('%ptype' => 'rows', '%wtype' => 'output')),
    '#default_value' => variable_get('regexpal_output_rows', 10),
  );
  $form['regexpal_default_settings_other']['regexpal_output_cols'] = array(
    '#type' => 'number',
    '#title' => t('Number of %ptype for %wtype textarea.', array('%ptype' => 'columns', '%wtype' => 'output')),
    '#default_value' => variable_get('regexpal_output_cols', 80),
  );

/*
  $form['regexpal_debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug stuff'),
    '#description' => t('Use those settings only for testing purposes!.'),
    '#collapsible' => TRUE,
  ); 

  $form['regexpal_debug']['regexpal_plain_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use separated javascript files.'),
    '#description' => t('Use plain javascript files for debugging purposes.'),
    '#default_value' => variable_get('regexpal_plain_js', FALSE),
  );
*/

  return system_settings_form($form); 
}
 
/**
 * Form API callback to validate the settings form.
 */
function regexpal_admin_form_validate($form, &$form_state) {
  $values = &$form_state['values'];
} 

