<?php

/**
 * @file
 *   Theme include file
 *
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * theme callback for quick reference
 */ 
function theme_regexpal_quick_reference() {
  $output = <<<HTML
	<div id="quickReference">
		<table cellspacing="0" summary="Regular expressions reference">
			<tbody>
				<tr>
					<td><code>.</code></td>
					<td>Any character except newline.</td>
				</tr>
				<tr class="altBg">
					<td><code>\.</code></td>
					<td>A period (and so on for <code>\*</code>, <code>\(</code>, <code>\\</code>, etc.)</td>
				</tr>
				<tr>
					<td><code>^</code></td>
					<td>The start of the string.</td>
				</tr>
				<tr class="altBg">
					<td><code>$</code></td>
					<td>The end of the string.</td>
				</tr>
				<tr>
					<td><code>\d</code>,<code>\w</code>,<code>\s</code></td>
					<td>A digit, word character <code>[A-Za-z0-9_]</code>, or whitespace.</td>
				</tr>
				<tr class="altBg">
					<td><code>\D</code>,<code>\W</code>,<code>\S</code></td>
					<td>Anything except a digit, word character, or whitespace.</td>
				</tr>
				<tr>
					<td><code>[abc]</code></td>
					<td>Character a, b, or c.</td>
				</tr>
				<tr class="altBg">
					<td><code>[a-z]</code></td>
					<td>a through z.</td>
				</tr>
				<tr>
					<td><code>[^abc]</code></td>
					<td>Any character except a, b, or c.</td>
				</tr>
				<tr class="altBg">
					<td><code>aa|bb</code></td>
					<td>Either aa or bb.</td>
				</tr>
				<tr>
					<td><code>?</code></td>
					<td>Zero or one of the preceding element.</td>
				</tr>
				<tr class="altBg">
					<td><code>*</code></td>
					<td>Zero or more of the preceding element.</td>
				</tr>
				<tr>
					<td><code>+</code></td>
					<td>One or more of the preceding element.</td>
				</tr>
				<tr class="altBg">
					<td><code>{<em>n</em>}</code></td>
					<td>Exactly <em>n</em> of the preceding element.</td>
				</tr>
				<tr>
					<td><code>{<em>n</em>,}</code></td>
					<td><em>n</em> or more of the preceding element.</td>
				</tr>
				<tr class="altBg">
					<td><code>{<em>m</em>,<em>n</em>}</code></td>
					<td>Between <em>m</em> and <em>n</em> of the preceding element.</td>
				</tr>
				<tr>
					<td><code>??</code>,<code>*?</code>,<code>+?</code>,<br/><code>{<em>n</em>}?</code>, etc.</td>
					<td>Same as above, but as few times as possible.</td>
				</tr>
				<tr class="altBg">
					<td><code>(</code><em>expr</em><code>)</code></td>
					<td>Capture <em>expr</em> for use with <code>\1</code>, etc.</td>
				</tr>
				<tr>
					<td><code>(?:</code><em>expr</em><code>)</code></td>
					<td>Non-capturing group.</td>
				</tr>
				<tr class="altBg">
					<td><code>(?=</code><em>expr</em><code>)</code></td>
					<td>Followed by <em>expr</em>.</td>
				</tr>
				<tr>
					<td><code>(?!</code><em>expr</em><code>)</code></td>
					<td>Not followed by <em>expr</em>.</td>
				</tr>
			</tbody>
		</table>
		<p><a href="http://www.regular-expressions.info/characters.html">Near-complete reference</a></p>
	</div>
HTML;
  return $output;
}

