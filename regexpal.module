<?php

/**
 * @file
 *   RegexPal is a JavaScript regular expression tester implemented in JavaScript and available at RegexPal.com
 *   It includes real-time regex syntax and match highlighting.
 *   The regex syntax highlighting is more complex than you might expect, as it strives to be aware of all aspects of ECMA-262 3rd Edition regex syntax,
 *   and some browser-specific bugs, features, and limitations.
 *
 * @version
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Implementation of hook_menu().
 */
function regexpal_menu() { 
  $items['regexpal'] = array(
    'title' => t('RegexPal Tester'),
    'description' => t('RegexPal is a JavaScript regular expression tester implemented in JavaScript.'),
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('regexpal_tester_form'),
    'access arguments' => array('access regexpal'),
    'file' => 'regexpal.form.inc', 
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/personalized_settings'] = array(
    'title' => t('RegexPal Settings'),
    'description' => t('RegexPal is a JavaScript regular expression tester implemented in JavaScript.'),
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('regexpal_admin_form'),
    'access arguments' => array('administer regexpal'),
    'file' => 'regexpal.admin.inc', 
  );

  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function regexpal_form_alter(&$form, $form_state, $form_id) {
}

/**
 * Implementation of hook_perm().
 */
function regexpal_perm() {
   return array('access regexpal', 'administer regexpal');
}

/**
 * Implementation of hook_block().
 *
 */
function regexpal_block($op = 'list', $delta = 0, $edit = array()) {
  // The $op parameter determines what piece of information is being requested.
  switch ($op) {
    case 'list':
      // If $op is "list", we just need to return a list of block descriptions.
      // This is used to provide a list of possible blocks to the administrator,
      // end users will not see these descriptions.
	  // You can override default settings: 'weight'     => 0, //'pages'      => 'node/*',
      $blocks['regexpal_tester'] = array(
        'info'       => t('RegexPal Tester'),
      );
      $blocks['regexpal_live_options'] = array(
        'info'       => t('RegexPal Options'),
      );
      $blocks['regexpal_quick_reference'] = array(
        'info'       => t('JavaScript Regex Quick Reference'),
      );
      return $blocks;
    case 'configure':
      $form = array();
/* Example:
      if ($delta == 0) {
        // All we need to provide is a text field, Drupal will take care of
        // the other block configuration options and the save button.
        $form['block_example_string'] = array(
          '#type' => 'textfield',
          '#title' => t('Block contents'),
          '#size' => 60,
          '#description' => t('This string will appear in the example block.'),
          '#default_value' => variable_get('block_example_string',  t('Some example content.')),
        );
      }
*/
      return $form;
    case 'save':
/*
      if ($delta == 0) {
        // Have Drupal save the string to the database.
        variable_set('block_example_string', $edit['block_example_string']);
      }
*/
      return;
    case 'view': default:
      module_load_include('form.inc', 'regexpal');
      switch ($delta) {
        case 'regexpal_tester':
          $block['subject'] = t('RegexPal Tester');
          $block['content'] = drupal_get_form('regexpal_tester_form');
          $block['region'] = 'content';
          $block['visibility'] = 1; // 1 = Show on only the listed pages.
          $block['pages'] = 'admin/settings/personalized_settings';
          $block['weight'] = 0;
          break;
        case 'regexpal_live_options':
          $block['subject'] = t('RegexPal Options');
          $block['content'] = drupal_get_form('regexpal_live_options_form');
          break;
        case 'regexpal_quick_reference':
          $block['subject'] = t('JavaScript Regex Quick Reference');
          $block['content'] = theme('regexpal_quick_reference');
          $block['region'] = 'right';
          $block['visibility'] = 1; // 1 = Show on only the listed pages.
          $block['pages'] = 'admin/settings/personalized_settings';
          $block['weight'] = 1;
          break;
      }
      return $block;
  }
}

/**
 * Implementation of hook_theme().
 */
function regexpal_theme() {
  return array(
    'regexpal_quick_reference' => array(
      'arguments' => array(),
      'file' => 'regexpal.theme.inc',
    ),
  );
}

