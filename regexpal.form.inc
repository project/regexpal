<?php

/**
 * @file
 *   Form include file
 *
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Implementation of drupal_get_form callback
 */
function regexpal_tester_form() {
  drupal_add_css(drupal_get_path('module', 'regexpal') . '/assets/regexpal.css');
  drupal_add_js(drupal_get_path('module', 'regexpal') . '/assets/xregexp.js', 'module', 'footer');
  drupal_add_js(drupal_get_path('module', 'regexpal') . '/assets/helpers.js', 'module', 'footer');
  drupal_add_js(drupal_get_path('module', 'regexpal') . '/assets/regexpal.js', 'module', 'footer');
  $form['regexpal']['regexpal_search'] = array(
    '#type' => 'textarea',
    //'#default_value' => variable_get('regexpal_form_id', ''),
    '#rows' => variable_get('regexpal_input_rows', 3),
    '#cols' => variable_get('regexpal_input_cols', 80),
    '#wysiwyg' => FALSE,
    '#prefix' => '<div id="search" class="smartField">',
    '#suffix' => '</div>',
  );
  $form['regexpal']['regexpal_input'] = array(
    '#type' => 'textarea',
    '#default_value' => t('Enter test data here.'),
    '#rows' => variable_get('regexpal_output_rows', 10),
    '#cols' => variable_get('regexpal_output_cols', 80),
    '#wysiwyg' => FALSE,
    '#prefix' => '<div id="input" class="smartField">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Implementation of drupal_get_form callback
 */
function regexpal_live_options_form() {
  module_load_include('admin.inc', 'regexpal');
  $form = regexpal_admin_form();
  $settings = $form['regexpal_default_settings'];
  foreach ($settings as $key => $option) {
    if (is_array($option)) {
      $settings[$key]['#attributes'] = array('title' => $settings[$key]['#description']); // conver description to title
      unset($settings[$key]['#description']); // and unset the description
    }
  }
  return $settings;
}

